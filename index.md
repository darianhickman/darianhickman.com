---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

<div id="pages" class="tabs">
        <ul>
          
          <li><span class="menu-background"></span><a class="menu-item" data-item-type="page" data-id="" href="https://darianhickman.github.io/">Portfolio</a></li>
          
          <li><span class="menu-background"></span><a class="menu-item" data-item-type="page" data-id="" href="https://villagethegame111.appspot.com">Village the Game</a></li>
          
          <li><span class="menu-background"></span><a class="menu-item" data-item-type="page" data-id="" href="http://www.linkedin.com/in/darianhickman">Darian on LinkedIn</a></li>
          
          <li><span class="menu-background"></span><a class="menu-item" data-item-type="page" data-id="" href="http://www.facebook.com/darian.hickman">Darian on Facebook</a></li>
          
          <li><span class="menu-background"></span><a class="menu-item" data-item-type="page" data-id="" href="https://www.goodreads.com/darianhickman">GoodReads</a></li>
          
          <li><span class="menu-background"></span><a class="menu-item" data-item-type="page" data-id="" href="http://www.quora.com/Darian-Hickman/questions">Questions on Quora</a></li>
          
        </ul>
 </div>